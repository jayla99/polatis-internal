module.exports = {
  verbose: true,
  moduleNameMapper: {
    "~(?!test)(.*)$": "<rootDir>/src$1",
    "~test(.*)$": "<rootDir>/test$1",
    "^react$": "preact/compat",
    "^react-dom$": "preact/compat"
  },
  setupFiles: ["./test/setup.js"],
  collectCoverageFrom: ["src/**/*.{js,jsx}"]
};
