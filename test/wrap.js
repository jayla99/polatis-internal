import * as Preact from "preact";

import { HashRouter } from "react-router-dom";
import { shallow, mount } from "enzyme";

import createTheme from "~/utils/theme";
import { ThemeProvider } from "styled-components";

export function mountWrap(node) {
  return mount(
    <ThemeProvider theme={createTheme}>
      <HashRouter>{node}</HashRouter>
    </ThemeProvider>
  );
}

export function shallowWrap(node) {
  return shallow(node, createContext());
}
