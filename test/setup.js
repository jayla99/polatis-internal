import "@babel/runtime/regenerator";

import chai from "chai";
import assertJsx, { options } from "preact-jsx-chai";
import { configure } from "enzyme";
import Adapter from "enzyme-adapter-preact-pure";

options.functions = false;

chai.use(assertJsx);

global.sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

configure({ adapter: new Adapter() });
