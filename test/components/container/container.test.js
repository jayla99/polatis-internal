import * as Preact from "preact";

import { expect } from "chai";
import { mountWrap } from "~test/wrap";
import Container from "~/components/container/container";

describe("Container", () => {
  it("should render the layout", () => {
    const wrapper = mountWrap(<Container title="hello">world</Container>);
    const header = wrapper.find("header");
    const main = wrapper.find("section");
    const footer = wrapper.find("footer");
    expect(header.find("h1").text()).to.equal("hello");
    expect(main.text()).to.equal("world");
    expect(footer.text()).to.include(new Date().getFullYear());
  });
});
