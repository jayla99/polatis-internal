zip.workerScriptsPath = "/lib/";

function onerror(e) {
  console.error("Error happened", e);
}

zip.createReader(
  new zip.HttpReader("bundle.zip"),
  function(zipReader) {
    zipReader.getEntries(function(entries) {
      var target = entries.length;
      var results = {};
      for (var i = 0; i < entries.length; i += 1) {
        (function(i) {
          entries[i].getData(
            new zip.TextWriter("utf8"),
            function(data) {
              results[entries[i].filename] = data;
              target -= 1;
              if (target === 0) {
                document.write(results["index.html"]);
              }
            },
            function() {},
            true
          );
        })(i);
      }
    });
  },
  onerror
);
