import path from "path";

import config from "./webpack.config";
import merge from "webpack-merge";
import HtmlPlugin from "html-webpack-plugin";

const basePath = path.join(__dirname, "..", "src");

export default merge(config, {
  mode: "development",
  plugins: [
    new HtmlPlugin({
      template: path.join(basePath, "index.html")
    })
  ]
});
