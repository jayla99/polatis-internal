import path from "path";

import webpack from "webpack";
import merge from "webpack-merge";
import config from "./webpack.config";

import HtmlPlugin from "html-webpack-plugin";
import HtmlWebpackInlineSourcePlugin from "html-webpack-inline-source-plugin";
import ZipPlugin from "zip-webpack-plugin";
import RemovePlugin from "remove-files-webpack-plugin";
import CopyPlugin from "copy-webpack-plugin";
import uglifyJs from "uglify-js";

const basePath = path.join(__dirname, "..", "src");

export default merge(config, {
  mode: "production",
  plugins: [
    new webpack.DefinePlugin({
      "process.env.NODE_ENV": JSON.stringify("production")
    }),
    new HtmlPlugin({
      template: path.join(basePath, "index.html"),
      inlineSource: ".(js|css)$"
    }),
    new HtmlWebpackInlineSourcePlugin(),
    new ZipPlugin({
      filename: "bundle.zip",
      include: [/\.html$/]
    }),
    new CopyPlugin([
      {
        from: path.join(basePath, "..", "unpacker"),
        to: path.join(basePath, "..", "build"),
        force: true,
        transform: (content, path) => {
          const test = /\.js$/gi;
          if (test.test(path)) {
            return uglifyJs.minify(content.toString()).code.toString();
          }
          return content;
        }
      }
    ]),
    new RemovePlugin({
      before: {
        include: [path.join(basePath, "..", "build")]
      },
      after: {
        include: [path.join(basePath, "..", "build", "main.js")]
      }
    })
  ]
});
