import path from "path";

const basePath = path.join(__dirname, "..", "src");
const env = process.env.NODE_ENV || "development";

console.log("Webpack running in " + env);

export default {
  entry: {
    main: path.join(basePath, "index.jsx")
  },

  output: {
    path: path.join(basePath, "..", "build"),
    publicPath: env === "development" ? "/" : "",
    filename: "[name].js"
  },

  resolve: {
    extensions: [".js", ".jsx"],
    alias: {
      react: "preact/compat",
      "react-dom": "preact/compat",
      "~": basePath
    }
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loader: "babel-loader",
        include: [basePath]
      },
      {
        test: /\.svg$/,
        loader: "svg-inline-loader"
      }
    ]
  },

  devServer: {
    noInfo: false,
    port: 8080,
    contentBase: path.join(basePath, "build"),
    historyApiFallback: {
      index: "/"
    }
  }
};
