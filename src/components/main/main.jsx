import * as Preact from "preact";

import styled from "styled-components";
import { map } from "styled-components-breakpoint";

const Container = styled.section`
  width: 100%;
  flex: 1 0 auto;
  ${props =>
    map(
      props.theme.container,
      container => `
    max-width: ${container.width};
    padding: ${container.padding};
  `
    )}
`;

function Main({ children }) {
  return <Container>{children}</Container>;
}

export default Main;
