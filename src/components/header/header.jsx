import * as Preact from "preact";

import _ from "~/utils/labels";
import { NavLink } from "react-router-dom";
import styled from "styled-components";
import { map } from "styled-components-breakpoint";

const Container = styled.header`
  margin: 0;
  background: ${props => props.theme.colors.primary};
  width: 100%;
  flex-shrink: 0;
`;

const Wrap = styled.div`
  margin: auto;
  display: flex;
  align-items: center;
  justify-content: space-between;
  ${props =>
    map(
      props.theme.container,
      container => `
    max-width: ${container.width};
    padding: ${container.padding};
  `
    )}
`;

const SwitchInfo = styled.div`
    height: 25px;
    width: 25px;
    background-color: ${props => props.theme.colors.button};
    display: inline-block;
    position: relative;
    bottom: 5px;
    border-radius: 50%;
  `

const SwitchText = styled.span`
    color: ${props => props.theme.colors.secondaryText};
    position: relative;
    display: inline-block;
    left: 10px;
    top: 5px;
    font-style: italic;
`

const HeaderLinkActive = btoa(Math.random()).substr(0, 8);
const HeaderLink = styled(NavLink).attrs({
  activeClassName: HeaderLinkActive
})`
  text-decoration: none;
  font-size: 12px;
  padding: 8px;
  color: ${props => props.theme.colors.primaryText};

  &.${HeaderLinkActive} {
    color: ${props => props.theme.colors.secondary};
  }
`;

const Title = styled.h1`
  margin: 0;
  color: ${props => props.theme.colors.primaryText};
`;

const Menu = styled.div``;

function Header() {
  return (
    <Container>
      <Wrap>
        <Title>{_("logo_text", {})}</Title>
        <Menu>
          <HeaderLink exact to="/settings">
          {_("user_name_link", {name: "(User)"})}
          </HeaderLink>
          <HeaderLink exact to="/">
           <SwitchInfo><SwitchText>{_("switch_info_button_text", {})}</SwitchText></SwitchInfo>
          </HeaderLink>
        </Menu>
      </Wrap>
    </Container>
  );
}

export default Header;