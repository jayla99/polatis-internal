import * as Preact from "preact";

import Header from "~/components/header/header";
import Footer from "~/components/footer/footer";
import Main from "~/components/main/main";

import styled from "styled-components";

const Wrap = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

function Container({ title, children }) {
  return (
    <Wrap>
      <Header title={title}></Header>
      <Main>{children}</Main>
      <Footer />
    </Wrap>
  );
}

export default Container;
