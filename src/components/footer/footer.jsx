import * as Preact from "preact";

import styled from "styled-components";

import { breakpoint, map } from "styled-components-breakpoint";

const Wrap = styled.footer`
  width: 100%;
  text-align: center;
  flex-shrink: 0;
  ${breakpoint("sm")`
    text-align: right;
  `}
  ${props =>
    map(
      props.theme.container,
      container => `
    max-width: ${container.width};
    padding: ${container.padding};
  `
    )}
`;

function Footer() {
  return <Wrap>{new Date().getUTCFullYear()} © All rights reserved</Wrap>;
}

export default Footer;
