import * as Preact from "preact";
import Container from "~/components/container/container";

import couch from "./couch.svg";

import styled from "styled-components";

import { useGlobalState } from "~/utils/global";

const SVGWrap = styled.div`
  position: relative;
  width: ${props => props.width}%;
  height: 0;
  padding-bottom: ${props => props.width}%;

  svg {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;

    path {
      fill: ${props => props.color};
    }

    path.phover:hover {
      fill: red;
      cursor: pointer;
    }
  }
`;

function SVG() {
  const [globalState, setGlobalState] = useGlobalState();
  const width = 49 + globalState.count;
  return (
    <Container title="SVG Test Page">
      <p>It feels like an SVG page</p>
      <p>Embed SVG from file:</p>
      <SVGWrap
        color="red"
        width={width}
        dangerouslySetInnerHTML={{ __html: couch }}
      />
      <p>Embed SVG:</p>
      <SVGWrap color="green" width={width}>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 100 100"
          preserveAspectRatio="none"
        >
          <g stroke="#1c1308" stroke-width="1">
            <path
              d="M32,29l-11-1c-25-1-20,0-18,20c2,8,0,13,10,13z"
              fill="#53381a"
            />
            <path
              d="M36,31h-11c-25-1-20,0-18,20c2,8,1,12,12,9z"
              fill="#8a5b28"
            />
            <path
              d="M32,25c5-10,3-12,18-10c20,5,20,6,35,6c10,1,20,4,10,16l-11,20h-66z"
              fill="#454f2e"
            />
            <path
              className="phover"
              onclick={() => setGlobalState({ count: globalState.count + 1 })}
              d="M13,53v7l45,18v-7zM32,25c5-10,3-12,15-8c20,6,10,6,31,6c10,1,18,4,10,16l-11,20l-56-9z"
              fill="#76894c"
            />
            <path
              d="M13,53c-3-10,2-9,10-10c10-1,37,7,20,7c-12,1-12,0-12,10zM31,60c-2-14,13-8,20-10l7,3v18z"
              fill="#8e996c"
            />
            <path
              d="M57,62c1,20,0,19,18,19c20,0,17,0,19-17c0-17-1-16-18-18c-20-1-19,0-19,16z"
              fill="#53381a"
            />
            <path
              d="M60,64c1,20,0,19,18,19c20,0,17,0,19-17c0-17-1-16-18-18c-20-1-19,0-19,16z"
              fill="#8a5b28"
            />
          </g>
        </svg>
      </SVGWrap>
    </Container>
  );
}

export default SVG;
