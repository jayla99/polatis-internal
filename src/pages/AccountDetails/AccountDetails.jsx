import * as Preact from "preact";
import _ from "~/utils/labels";

import styled from "styled-components";
import Container from "~/components/container/container";


const SettingsWrapper = styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    align-items: center;
`
const AccountSettingsHeader = styled.h4`
    text-decoration: underline;
`

const AccountSettingsPanel = styled.div`
    width: 45%;
    margin: 2.5%;
    min-width: 320px;
    height: 400px;
    background-color: ${props => props.theme.colors.material};
    border: ${props => `1px solid ` + props.theme.colors.border};
`

const AccountSettingsInput = styled.input`
    background-color: ${props => props.theme.colors.altInput};
    border: ${props => `1px solid ` + props.theme.colors.border};
    padding: 10px;
    margin: 5%;
    width: 90%;
    border: 0;
`
const PanelText = styled.h4`
    text-align: center;
`

function AccountDetails(){
    return (
        <Container>
            <AccountSettingsHeader>{_("account_details_header", {})}</AccountSettingsHeader>
            <SettingsWrapper>
                <AccountSettingsPanel>
                    <PanelText>{_("account_details_header", {})}</PanelText>
                    <AccountSettingsInput placeholder={_("username", {})}/>
                    <AccountSettingsInput placeholder={_("user_group", {})}/>
                    <PanelText>{_("last_logged_in_detail", {dateTime: "15:30 02.03.2020"})}</PanelText>
                </AccountSettingsPanel>
                <AccountSettingsPanel>
                    <PanelText>{_("change_password_header", {})}</PanelText>
                    <AccountSettingsInput placeholder={_("existing_password", {})}/>
                    <AccountSettingsInput placeholder={_("new_password", {})}/>
                    <AccountSettingsInput placeholder={_("confirm_new_password", {})}/>
                </AccountSettingsPanel>
            </SettingsWrapper>
        </Container>
    )
}

export default AccountDetails;