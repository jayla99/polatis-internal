import * as Preact from "preact";
import styled from "styled-components";
import Container from "~/components/container/container";

const Wrap = styled.div`
  padding-top: 5%;
  padding-bottom: 10%;
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

function Dashboard(){
    return(
        <Container title="H+S Polatis Logo">
          <Wrap>
          </Wrap>
        </Container>
    )
}

export default Dashboard;