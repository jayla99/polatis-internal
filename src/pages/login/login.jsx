import * as Preact from "preact";
import styled from "styled-components";
import _ from "~/utils/labels";
import Container from "~/components/container/container";

const Wrap = styled.div`
  padding-top: 5%;
  padding-bottom: 10%;
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const LoginHeader = styled.h3`
    margin: 15px 0;
    text-align: center;
`
const LoginInput = styled.input`
    background-color: ${props => props.theme.colors.input};
    padding: 10px;
    margin: 10px 0;
    width: 50%;
    border: 0;
`
const LoginButton = styled.button`
    background-color: ${props => props.theme.colors.button};
    color: ${props => props.theme.colors.secondaryText};
    border: 0;
    margin: 5px 0;
    width: 50%;
    height: 30px;
`
function Login(){
    return(
        <Container>
          <Wrap>
            <LoginHeader>{_("login_header", {})}</LoginHeader>
            <LoginInput placeholder={_("username", {})}/>
            <LoginInput placeholder={_("password", {})}/>
            <LoginButton>{_("login_button_text", {})}</LoginButton>
          </Wrap>
        </Container>
    )
}

export default Login;