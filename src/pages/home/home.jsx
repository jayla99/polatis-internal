import * as Preact from "preact";

import Container from "~/components/container/container";

import { useGlobalState } from "~/utils/global";
import _ from "~/utils/labels";

import { useState, useEffect } from "preact/hooks";

import styled from "styled-components";

import * as api from "~/utils/api.js";

const Wrap = styled.div`
  * {
    margin-bottom: 16px;
  }
`;

function Home() {
  const [globalState, setGlobalState] = useGlobalState();

  const [state, setState] = useState({ count: 1 });

  useEffect(async () => {
    const testValue = await api.testApiCall();
    setState({ count: testValue });
  }, []);

  return (
    <Container title="Home">
      <Wrap>
        <span>{_("test_label", { count: globalState.count })}</span>
        <div>
          <button
            type="button"
            onClick={() => setGlobalState({ count: globalState.count + 1 })}
          >
            +1
          </button>
          <button
            type="button"
            onClick={() => setGlobalState({ count: globalState.count - 1 })}
          >
            -1
          </button>
        </div>
        <span>Testing Local State: {state.count}</span>
        <div>
          <button
            type="button"
            onClick={() => setState({ count: state.count + 1 })}
          >
            +1
          </button>
          <button
            type="button"
            onClick={() => setState({ count: state.count - 1 })}
          >
            -1
          </button>
        </div>
      </Wrap>
    </Container>
  );
}

export default Home;
