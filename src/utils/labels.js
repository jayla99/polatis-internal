import labels from "~/labels.json";

import { render } from "mustache";

export default function _(template, params) {
  return render(labels[template], params);
}
