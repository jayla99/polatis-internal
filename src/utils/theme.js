module.exports = function() {
  return {
    breakpoints: {
      xs: 0,
      sm: 576,
      md: 768,
      lg: 992,
      xl: 1200
    },
    container: {
      xs: {
        width: "100%",
        padding: "8px"
      },
      sm: {
        width: "750px",
        padding: "16px"
      },
      md: {
        width: "970px",
        padding: "16px"
      },
      lg: {
        width: "1170px",
        padding: "16px"
      },
      xl: {
        width: "1170px",
        padding: "16px"
      }
    },
    colors: {
      primary: "#FFDE03",
      secondary: "#0336FF",
      primaryText: "rgba(0, 0, 0, 0.8)",
      secondaryText: "#fff",
      material: "#fff",
      input: "#fff",
      altInput: "#e5e5e5",
      button: "#000",
      border: "#000",
      background: "rgb(229, 229, 229)"
    }
  };
};
