import * as Preact from "preact";

import { useState, useLayoutEffect } from "preact/hooks";

import { Subject } from "rxjs";

const subject = new Subject();

const initialState = {
  count: 1,
  text: "hello"
};

let state = initialState;

const globalStore = {
  init: () => subject.next(state),
  subscribe: setState => subject.subscribe(setState),
  setState: newState => {
    state = {
      ...state,
      ...newState
    };
    subject.next(state);
  },
  initialState
};

export function useGlobalState() {
  const [preactState, preactSetState] = useState(state);

  useLayoutEffect(() => {
    globalStore.subscribe(preactSetState);
    globalStore.init();
  }, []);

  return [preactState, globalStore.setState];
}
