import "./polyfill";

import * as Preact from "preact";
import { HashRouter as Router, Route } from "react-router-dom";

import Home from "~/pages/home/home";
import SVG from "~/pages/svg/svg";

import AccountDetails from "~/pages/AccountDetails/AccountDetails";
import Login from "~/pages/login/login";

import { createGlobalStyle, ThemeProvider } from "styled-components";

import createTheme from "~/utils/theme";

const theme = createTheme();

const GlobalStyle = createGlobalStyle`
  html {
    height: 100%;
  }
  body {
    margin: 0px;
    font-family: Arial, Helvetica, sans-serif;
    color: ${props => props.theme.colors.primaryText};
    background: ${props => props.theme.colors.background};
    height: 100%;
  }
  #root {
    height: 100%;
  }
  * {
    box-sizing: border-box;
  }
`;

const renderApp = () => {
  Preact.render(
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <Router>
        <Route exact path="/" component={Home} />
        <Route exact path="/accountdetails" component={AccountDetails} />
        <Route exact path="/login" component={Login} />
      </Router>
    </ThemeProvider>,
    document.getElementById("root")
  );
};

renderApp();
